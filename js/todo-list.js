// declare elements
var taskArray = [];
var modal;
var categoryModelBox;
var categoryBox;



function loadCommonData() {
    // Get the modal
    modal = document.getElementById('myModal');
    categoryModelBox = document.getElementById('categoryModelBox');
    var closeCategory = document.getElementById('closeCategory');

    var close = document.getElementById('close');
    var saveChange = document.getElementById('saveChange');

    categoryBox = document.querySelector(".categoryBox");


    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");
    var addCateogry = document.getElementById("addCateogry");



    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    close.onclick = function () {
        modal.style.display = "none";
    }
    closeCategory.onclick = function () {
        categoryModelBox.style.display = "none";
    }


    // When the user clicks theAdd Task button, open the modal 
    btn.onclick = function () {
        modal.style.display = "block";
    }

    // When the user clicks theAdd Task button, open the modal 
    addCateogry.onclick = function () {
        categoryModelBox.style.display = "block";
    }



    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }


}


function myFunction() {
    loadCommonData();

}

function calculateTasksByCateogry(id) {
    var length = 1;
    for (i = 0; i < taskArray.length; i++) {
        if (taskArray[i].parentId == id) {
            length++;
        }


    }
    
    return length
}

function saveCategory() {
    
    var category = document.getElementById("category");
    var newDiv = document.createElement("div");
    newDiv.id = category.value;
    var newContent = document.createTextNode(category.value);
    newDiv.addEventListener("click", edit_addon);
    newDiv.appendChild(newContent);

    // create span 
    var span = document.createElement("span");
    var countTxt = document.createTextNode("0");
    span.style.backgroundColor = "lightblue";
    span.style.cssFloat="center"
    span.appendChild(countTxt);
    newDiv.appendChild(span);


    categoryBox.appendChild(newDiv); + "<br>";

    categoryModelBox.style.display = "none";

    // clear task and deadline date
    category.value = '';

}

function edit_addon(event) {
    div_id = this.id;
    var categoryBox = document.getElementById(div_id);
    var hiddenCat = document.getElementById("hiddenCat");

    if (hiddenCat.innerText != '') {
        var ff = document.getElementById(hiddenCat.innerText);
        ff.style.backgroundColor = "#ECF0F1";
    }
    hiddenCat.innerText = div_id;
    categoryBox.style.backgroundColor = "green";

    for (i = 0; i < taskArray.length; i++) {
        if (taskArray[i].parentId) {
            displayTasksByCategory(this.id);
        }
    }

}

function saveTask() {
    var hiddenCat = document.getElementById("hiddenCat");

    var taskId = document.getElementById("taskId");
    var deadlineDate = document.getElementById("deadlineDate");
    var tasksPerCateogry = calculateTasksByCateogry(hiddenCat.innerText)

    var task = { name: taskId.value, date: deadlineDate.value, parentId: hiddenCat.innerText };
    taskArray.push(task);
    dynamicLoadTable(task);

    var selectedCategory = document.getElementById(hiddenCat.innerText);
    selectedCategory.lastChild.textContent = tasksPerCateogry;


    modal.style.display = "none";

    // clear task and deadline date
    taskId.value = '';
    deadlineDate.value = '';

}

function handleClick(cb) {

    // add strike code

}


function dynamicLoadTable(task) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    cell1.innerHTML = "<input type=\"checkbox\" id=\"complete\" value=\"no\" onclick=\"handleClick(this);\">";
    cell2.id = task.parentId;
    cell2.innerHTML = task.name;
    cell3.innerHTML = task.date;

}

function filterTable() {
    var input, filter, table, tr, td, i;
    input = document.getElementById("mysearch");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];

        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


function displayTasksByCategory(div_id) {
    var input, filter, table, tr, td, i;
    input = document.getElementById("mysearch");
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {


            if (td.id.indexOf(div_id) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function clearAll() {
    $("#myTable").children().remove()
    var selectedCategory = document.getElementById(hiddenCat.innerText);
    selectedCategory.lastChild.textContent = 0;

}




