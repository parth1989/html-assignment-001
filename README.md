# Todo Application #

## What you have to do? ##

* You have to develop a Todo list app.
* It is only required to be develop for desktop version.
* Open the images and use them to use images, colors, sizes, fonts...
* The modals should be shown in the same page as the main app and only on demand using Javascript.

## Hints ##

* Make use of standard semantic html tags to build the page structure.
* Make sure to test your mark up through Markup Validation Service for html errors before starting the CSS.
* Add the modals and hide them so you can show them using Javascript.
* Use CSS3 to style the app according to the designs.
* Make use of Sprites for icons, arrows, small buttons.
* Upload your page on a web server to test it using Responsinator or make use of Responsive layout which comes with Firefox/Chrome web developer tools to test your pages in different screen locally.
* Test your page in Internet Explorer 10 and up, Chrome and Firefox.